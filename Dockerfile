FROM centos:latest

MAINTAINER Daniel, danielfrancora@gmail.com

# Upgrading system
RUN yum -y install epel-release
RUN yum -y update \
    & yum -y install python-pip python-dev

COPY requirements.txt /
RUN pip install -r requirements.txt

COPY src/api/app.py /
EXPOSE 5000
CMD [ "nohup python /app.py &" ]